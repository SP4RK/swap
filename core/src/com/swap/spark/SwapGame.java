package com.swap.spark;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.swap.spark.Controller.GameState;
import com.swap.spark.Controller.GameColorType;
import com.swap.spark.View.GameColor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class SwapGame extends Game{
    private static SwapGame ourInstance = new SwapGame();
    private SpriteBatch batch;
    public com.swap.spark.View.Screens.GameMenuScreen gameMenuScreen;
    public com.swap.spark.View.Screens.SettingsScreen settingsScreen;
    public com.swap.spark.View.Screens.AboutScreen aboutScreen;
    public com.swap.spark.View.Screens.GameScreen gameScreen;
    public com.swap.spark.View.Screens.GameOverScreen gameOverScreen;
    private float ppuX, ppuY;
    public static float GAME_HEIGHT = 665f;
    public static float GAME_WIDTH = 373f;
    public int Score = 0;
    public File file;
    public int HighScore;
    public boolean Pause = false;
    public GameState gameState = GameState.GAME_READY;
    private ShapeRenderer shapeRenderer;

    public static SwapGame getInstance()
    {
        return ourInstance;
    }

    public SwapGame() {
    }

    @Override
    public void create() {
        ppuX = Gdx.graphics.getWidth()/GAME_WIDTH;
        ppuY = Gdx.graphics.getHeight()/GAME_HEIGHT;
        batch = new SpriteBatch();
        shapeRenderer = new ShapeRenderer();
        gameMenuScreen = new com.swap.spark.View.Screens.GameMenuScreen(batch);
        settingsScreen = new com.swap.spark.View.Screens.SettingsScreen(batch);
        aboutScreen = new com.swap.spark.View.Screens.AboutScreen(batch);
        gameOverScreen = new com.swap.spark.View.Screens.GameOverScreen(batch);
        try {
            gameScreen = new com.swap.spark.View.Screens.GameScreen(batch);
        } catch (IOException e) {
            e.printStackTrace();
        }

        setScreen(gameMenuScreen);
    }

    public void setGameColorType(GameColorType gameColorType){
        GameColor.setGameColor(gameColorType);
    }

    public void moveToGameMenu(){setScreen(gameMenuScreen);}
    public void moveToSettings(){setScreen(settingsScreen);}
    public void moveToAbout(){setScreen(aboutScreen);}
    public void moveToGame(){setScreen(gameScreen);}
    public void moveToGameOver(){setScreen(gameOverScreen);}

    public float getPpuX() {
        return ppuX;
    }
    public void setPpuX(float ppuX) {
        this.ppuX = ppuX;
    }
    public boolean getPause() {
        return Pause;
    }

    public void unpause () {
        Pause = false;
    }
    public void pause() {
        Pause = true;
    }

    public void getHighScore() throws IOException {
        file = new File(resolvePath("HighScore.txt"));
        if (!file.exists())
            file.createNewFile();
        Scanner sc = new Scanner(file);
        String temp;
            temp = sc.nextLine();
            HighScore = Integer.parseInt(temp);
        sc.close();
    }
    public void setHighScore() throws IOException {
        int temp = SwapGame.getInstance().getScore();
        if (temp >= HighScore){
            HighScore = temp;
            file = new File(resolvePath("HighScore.txt"));
            if (!file.exists())
                file.createNewFile();
            FileWriter out = new FileWriter(file, false);
            out.write(String.format(("%s"), temp));
            out.close();
        }
    }

    public int getScore() {
        return Score;
    }
    public void setScore(int score) {
        this.Score = score;
    }
    public float getPpuY() {
        return ppuY;
    }
    public void setPpuY(float ppuY) {
        this.ppuY = ppuY;
    }

    public ShapeRenderer getShapeRenderer() {
        return shapeRenderer;
    }

    public String resolvePath (String fileName){
        return String.format(
                Gdx.app.getType() == Application.ApplicationType.Android ?
                        fileName :
                        String.format("android/assets/%s", fileName)
        );
    }
}



