package com.swap.spark.Model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.swap.spark.Controller.ColorType;
import com.swap.spark.Controller.GameState;
import com.swap.spark.SwapGame;
import com.swap.spark.View.Bot;
import com.swap.spark.View.Player;
import com.swap.spark.View.ScoreFontActor;

import java.io.File;
import java.io.IOException;

public class World extends Stage {
    private Player player;
    public Bot bot;
    public ScoreFontActor score;
    public float botSpeed;
    public float speed;
    public float botCurrentTime;

    public World(ScreenViewport screenViewport, SpriteBatch batch) throws IOException {
        super(screenViewport, batch);
        SwapGame.getInstance().file = new File(SwapGame.getInstance().resolvePath("HighScore.txt"));
        if (!SwapGame.getInstance().file.exists())
            try {
                SwapGame.getInstance().setHighScore();
            } catch (IOException e) {
                e.printStackTrace();
            }

        setPlayer(new Player(Gdx.graphics.getWidth() / 2, 75, ColorType.BLUE_PINK, 42, SwapGame.getInstance().getShapeRenderer()));
        addActor(player);
        botCurrentTime = 0;
        botSpeed = 0.6f; // чем меньше, тем быстрее спавнятся
        speed = 321; // чем меньшее, тем медленнее двигаются
        score = new ScoreFontActor(true, 170, 641, 38, Color.BLACK, "FuturaHeavy.ttf");
        addActor(score);
    }

    public void act(float delta) {
        super.act(delta);

        botCurrentTime+=delta;
        if(botCurrentTime >= botSpeed){
            botCurrentTime = 0;
            bot = new Bot(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() + 25, ColorType.getRandom(), 25, SwapGame.getInstance().getShapeRenderer(), speed, player);
            addActor(bot);
        }

    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void restart() {
        Actor[] actors = getActors().toArray();
        for (Actor actor : actors)
            if (actor.getName() != null && actor.getName().equals("bot")) {
                actor.remove();
            }
    }

    public void start() throws IOException {
        player.setState(ColorType.PINK_BLUE);
        SwapGame.getInstance().gameState = GameState.GAME_RUNNING;
        SwapGame.getInstance().getHighScore();
        SwapGame.getInstance().setScore(0);
        restart();
    }
}


