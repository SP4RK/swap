package com.swap.spark.Model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.swap.spark.Controller.MoveTo.MoveToMenu;
import com.swap.spark.Controller.RestartGame;
import com.swap.spark.Controller.Unpause;
import com.swap.spark.SwapGame;
import com.swap.spark.View.CentredFontActor;
import com.swap.spark.View.GameColor;
import com.swap.spark.View.ImageActor;
import com.swap.spark.View.ShapeActor;

import java.io.IOException;

public class Pause extends Stage {
    private ImageActor background;
    private ShapeActor continueButton;
    private ShapeActor restartButton;
    private ShapeActor backToMenuButton;
    private CentredFontActor pauseFont;
    private CentredFontActor continueFont;
    private CentredFontActor restartFont;
    private CentredFontActor backToMenuFont;
    private ImageActor settingsSound;
    private ImageActor settingsSoundMuted;


    public Pause(ScreenViewport screenViewport, SpriteBatch batch) throws IOException {
        super(screenViewport, batch);
        //background = new ImageActor(new Texture(SwapGame.getInstance().resolvePath("bluredBackground.png")),0, 0, 373, 665);
        continueButton = new ShapeActor(114, 268, 145, 43, GameColor.BLUE_BUTTON);
        restartButton = new ShapeActor(114, 208, 145, 43, GameColor.LIGHTBLUE_BUTTON);
        backToMenuButton = new ShapeActor (114, 148, 145, 43, GameColor.PURPLE_BUTTON);
        continueButton.addListener(new Unpause());
        pauseFont = new CentredFontActor("P A U S E", 102, 490, 42, Color.BLACK, "FuturaHeavy.ttf");
        continueFont = new CentredFontActor("Continue",151, 296.5f, 18,Color.WHITE,"FuturaHeavy.ttf");
        continueFont.setPosition(continueFont.getX(), continueButton.getY() + (continueButton.getHeight() - continueFont.getHeight())/7 );
        restartFont = new CentredFontActor("Restart", 157.5f, 237, 18, Color.WHITE, "FuturaHeavy.ttf");
        restartFont.setPosition(restartFont.getX(), restartButton.getY() + (restartButton.getHeight() - restartFont.getHeight())/7 );
        backToMenuFont = new CentredFontActor("Back to Menu", 132, 176, 18, Color.WHITE, "FuturaHeavy.ttf");
        backToMenuFont.setPosition(backToMenuFont.getX(), backToMenuButton.getY() + (backToMenuButton.getHeight() - backToMenuFont.getHeight())/7 );
        restartButton.addListener(new RestartGame());
        backToMenuButton.addListener(new MoveToMenu());

        settingsSound = new ImageActor(new Texture(SwapGame.getInstance().resolvePath("settings_sound.png")),161, 20, 50, 50);
        settingsSound.addListener(new VolumeChangeOff());
        settingsSoundMuted = new ImageActor(new Texture(SwapGame.getInstance().resolvePath("settings_sound_muted.png")),161, 20, 50, 50);
        settingsSoundMuted.addListener(new VolumeChangeOn());
        settingsSoundMuted.setVisible(false);

        //addActor(background);
        addActor(continueButton);
        addActor(restartButton);
        addActor(backToMenuButton);
        addActor(continueFont);
        addActor(restartFont);
        addActor(backToMenuFont);
        addActor(pauseFont);
        addActor(settingsSound);
        addActor(settingsSoundMuted);
    }

    public void act(float delta) {
        super.act(delta);
    }

    class VolumeChangeOff extends ClickListener {
        public void clicked (InputEvent event, float x, float y) {
            settingsSoundMuted.setVisible(true);
        }
    }
    class VolumeChangeOn extends ClickListener {
        public void clicked (InputEvent event, float x, float y) {
            settingsSoundMuted.setVisible(false);
        }
    }
}
