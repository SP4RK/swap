package com.swap.spark.Model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.swap.spark.Controller.Pause;
import com.swap.spark.SwapGame;
import com.swap.spark.View.ImageActor;

import java.io.IOException;

public class UI extends Stage {
    public ImageActor PauseButton;

    public UI(ScreenViewport screenViewport, SpriteBatch batch) throws IOException {
        super(screenViewport, batch);
        PauseButton = new ImageActor(new Texture(SwapGame.getInstance().resolvePath("pause.png")),20, 615);
        PauseButton.addListener(new Pause());
        addActor(PauseButton);
    }

    public void act(float delta) {
        super.act(delta);
    }
}
