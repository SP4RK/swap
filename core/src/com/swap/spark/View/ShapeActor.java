package com.swap.spark.View;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.swap.spark.SwapGame;

public class ShapeActor extends Actor  {
    ShapeRenderer shapeRenderer;
    private boolean circle_rrect = true;
    private float X;
    private float Y;
    private float Width;
    private float Height;
    private Circle circle;
    private Circle circle2;
    private Color color;
    private Touchable touchable = Touchable.enabled;

    public ShapeActor (float x, float y, float width, float height, Color color){
        shapeRenderer = new ShapeRenderer();
        X = x;
        Y = y;
        Width = width;
        Height = height;
        this.color = color;
        Height *= SwapGame.getInstance().getPpuY();
        Width *= SwapGame.getInstance().getPpuX();
        X+= Height/2;
        X *= SwapGame.getInstance().getPpuX();
        Y+= Height/2;
        Y *= SwapGame.getInstance().getPpuY();
        //circle = new Circle(X, Y, Height/2-4.2f * SwapGame.getInstance().getPpuX(), color);
        //circle2 = new Circle(X+Width-Height, Y, Height/2-4.2f * SwapGame.getInstance().getPpuX(), color);
        X = (Gdx.graphics.getWidth() - Width + Height)/2;
        setPosition(X, Y);
        setSize(Width, Height);
    }

    public void draw (Batch batch, float parentAlpha){
        batch.end();
        shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(color);
        //circle.draw();
        shapeRenderer.circle(X, Y, Height/2);
        shapeRenderer.rect(X, Y - Height / 2, Width - Height, Height);
        shapeRenderer.circle(X+Width-Height, Y, Height/2);
        //circle2.draw();
        shapeRenderer.end();
        batch.begin();
    }

    public void moveBy(float x, float y) {
        if(!circle_rrect) {
            shapeRenderer.translate(this.getX() + x, this.getY() + y, 0);
            circle.setY(circle.getY() + y);
        }
    }

    public Actor hit (float x, float y, boolean touchable) {
        if (touchable && this.touchable != Touchable.enabled) return null;
        return x >= 0 - Height/2 && x < Width - Height/2 && y >= 0 - Height/2 && y < Height/2 ? this : null;
    }
}
