package com.swap.spark.View;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.swap.spark.Controller.ColorType;
import com.swap.spark.SwapGame;

import static com.swap.spark.Controller.ColorType.BLUE_PINK;
import static com.swap.spark.Controller.ColorType.BLUE_TRANSPARENT;
import static com.swap.spark.Controller.ColorType.PINK_BLUE;
import static com.swap.spark.Controller.ColorType.PINK_TRANSPARENT;
import static com.swap.spark.Controller.ColorType.TRANSPARENT_BLUE;
import static com.swap.spark.Controller.ColorType.TRANSPARENT_PINK;

public class Player extends Actor {
    private ColorType state;
    float radius;
    public ShapeRenderer shape;
    public Rectangle borders;
    public float a = 2.5f;

    public Player(float x, float y, ColorType colorType, float radius, ShapeRenderer shapeRenderer){
        setPosition(x, y);
        shape = shapeRenderer;
        this.radius = radius * SwapGame.getInstance().getPpuX();
        borders = new Rectangle(x - 4*this.radius, y, 8*this.radius, 0);
        setState(PINK_BLUE);
    }

    public Rectangle getBorders(){
        return borders;
    }

    public ColorType getState() {
        return state;
    }

    public void setState(ColorType state) {
        this.state = state;
    }

    public void swap(){
        switch (state){
            case BLUE_PINK:
                setState(PINK_BLUE);
                break;
            case PINK_BLUE:
                setState(BLUE_PINK);
                break;
        }
    }
    public void swipeLeft(){
        switch (state){
            case BLUE_PINK:
                setState(PINK_TRANSPARENT);
                break;
            case PINK_BLUE:
                setState(BLUE_TRANSPARENT);
                break;
            case TRANSPARENT_PINK:
                setState(PINK_BLUE);
                break;
            case TRANSPARENT_BLUE:
                setState(BLUE_PINK);
                break;
        }
    }
    public void swipeRight(){
        switch (state){
            case BLUE_PINK:
                setState(TRANSPARENT_BLUE);
                break;
            case PINK_BLUE:
                setState(TRANSPARENT_PINK);
                break;
            case PINK_TRANSPARENT:
                setState(BLUE_PINK);
                break;
            case BLUE_TRANSPARENT:
                setState(PINK_BLUE);
                break;
        }
    }
    public void drawCircle(ShapeRenderer shape,float a, float x, float y, float radius, Color cl) {
        shape.begin(ShapeRenderer.ShapeType.Filled);
        shape.setColor(cl);
        shape.circle(x, y, radius);
        shape.end();
    }
    public void draw(Batch batch, float parentAlpha){
        shape.setProjectionMatrix(batch.getProjectionMatrix());
        batch.end();
        switch(state){
            case BLUE_PINK:
                setState(BLUE_PINK);
                drawCircle(shape, a, getX() - a * radius, getY(), radius, GameColor.FIRST_CIRCLE);
                drawCircle(shape,a, getX()+a*radius, getY(), radius, GameColor.SECOND_CIRCLE);
                break;
            case PINK_BLUE:
                setState(PINK_BLUE);
                drawCircle(shape,a, getX()-a*radius, getY(), radius, GameColor.SECOND_CIRCLE);
                drawCircle(shape,a, getX()+a*radius, getY(), radius, GameColor.FIRST_CIRCLE);
                break;
            case BLUE_TRANSPARENT:
                setState(BLUE_TRANSPARENT);
                drawCircle(shape,a, getX()-a*radius, getY(), radius, GameColor.FIRST_CIRCLE);
                break;
            case TRANSPARENT_BLUE:
                setState(TRANSPARENT_BLUE);
                drawCircle(shape,a, getX()+a*radius, getY(), radius, GameColor.FIRST_CIRCLE);
                break;
            case PINK_TRANSPARENT:
                setState(PINK_TRANSPARENT);
                drawCircle(shape,a, getX()-a*radius, getY(), radius, GameColor.SECOND_CIRCLE);
                break;
            case TRANSPARENT_PINK:
                setState(TRANSPARENT_PINK);
                drawCircle(shape,a, getX()+a*radius, getY(), radius, GameColor.SECOND_CIRCLE);
                break;
        }
        batch.begin();
    }


}
