package com.swap.spark.View;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.swap.spark.SwapGame;

public class CircleActor extends Actor {
    private float X;
    private float Y;
    private float Radius;
    private Circle circle;
    private Circle circle2;
    private Color color1;
    private Color color2;
    private ShapeRenderer shape;

    public CircleActor (float x, float y, float radius, Color color1, Color color2){
        this.color1 = color1;
        this.color2 = color2;
        X = x * SwapGame.getInstance().getPpuX();
        Y = y * SwapGame.getInstance().getPpuX();
        Radius = radius * SwapGame.getInstance().getPpuX();
        shape = SwapGame.getInstance().getShapeRenderer();
        //circle = new Circle(X - 1.47f*Radius, Y + 1.27f*Radius, Radius, color1);
        //circle2 = new Circle(X + 1.47f*Radius, Y - 1.27f*Radius, Radius, color2);
    }

    public void drawCircle(ShapeRenderer shape, float x, float y, float radius, Color color) {
        shape.begin(ShapeRenderer.ShapeType.Filled);
        shape.setColor(color);
        shape.circle(x, y, radius);
        shape.end();
    }

    public void draw (Batch batch, float parentAlpha){
        shape.setProjectionMatrix(batch.getProjectionMatrix());
        batch.end();
        drawCircle(shape, X - 1.47f*Radius, Y + 1.27f*Radius, Radius, color1);
        drawCircle(shape, X + 1.47f*Radius, Y - 1.27f*Radius, Radius, color2);
        batch.begin();
    }
}

