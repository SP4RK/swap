package com.swap.spark.View;

import com.badlogic.gdx.graphics.Color;
import com.swap.spark.Controller.GameColorType;

/**
 * Created by 10k1103 on 26.03.2016.
 */
public class GameColor extends Color{

    private static GameColorType gameColorType;
    public static Color FIRST_CIRCLE = new Color(131f / 255f, 147f / 255f, 202f / 255f, 1);
    public static Color SECOND_CIRCLE = new Color(1, 169f/255f, 214f/255f, 1);
    public static final Color PURPLE_BUTTON = new Color (114f / 255f, 74f / 255f, 148f / 255f, 1);
    public static final Color PINK_BUTTON = new Color (240f / 255f, 138f / 255f, 183f / 255f, 1);
    public static final Color BLUE_BUTTON = new Color (77f / 255f, 92f / 255f, 179f / 255f, 1);
    public static final Color LIGHTBLUE_BUTTON = new Color (105f / 255f, 201f / 255f, 237f / 255f, 1);
    public static final Color GREEN_BUTTON = new Color (86f / 255f, 214f / 255f, 200f / 255f, 1);
    public static final Color GRAY_BUTTON = new Color (181f / 255f, 181f / 255f, 181f / 255f, 1);
    public static final Color GRAYVOLUME_BUTTON = new Color (139f / 255f, 139f / 255f, 139f / 255f, 1);
    public static final Color BLACK = new Color (0, 0, 0, 1);

    public static void setGameColor(GameColorType gameColor){
        gameColorType = gameColor;
        updateColors();
    }

    private static void updateColors(){
        switch (gameColorType){
            case BLACK_WHITE:
                FIRST_CIRCLE = DARK_GRAY;
                SECOND_CIRCLE = LIGHT_GRAY;
                //FIRST_CIRCLE = new Color(112f / 255f, 89f / 255f, 255f / 255f, 1);
                //SECOND_CIRCLE = new Color(99f/255f, 235f/255f, 195f/255f, 1);
                break;
            case PINK_BLUE:
                FIRST_CIRCLE = new Color(131f / 255f, 147f / 255f, 202f / 255f, 1);
                SECOND_CIRCLE = new Color(1, 169f/255f, 214f/255f, 1);
                break;
        }
    }
}
