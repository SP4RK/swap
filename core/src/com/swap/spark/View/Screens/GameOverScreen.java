package com.swap.spark.View.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.swap.spark.Controller.MoveTo.MoveToGame;
import com.swap.spark.Controller.MoveTo.MoveToMenu;
import com.swap.spark.View.CentredFontActor;
import com.swap.spark.View.GameColor;
import com.swap.spark.View.ScoreFontActor;
import com.swap.spark.View.ShapeActor;

public class GameOverScreen implements Screen {
    private Stage stage;
    private ShapeActor playAgainButton;
    private ShapeActor shareButton;
    private ShapeActor backToMenuButton;
    //private CircleActor circles;
    public ScoreFontActor score;
    public ScoreFontActor highscore;
    public CentredFontActor yourScore;
    public CentredFontActor bestScore;
    private CentredFontActor playAgainFont;
    private CentredFontActor shareFont;
    private CentredFontActor backToMenuFont;

    public GameOverScreen(SpriteBatch batch) {
        playAgainButton = new ShapeActor(114, 268, 145, 43, GameColor.BLUE_BUTTON);
        shareButton = new ShapeActor(114, 208, 145, 43, GameColor.PINK_BUTTON);
        backToMenuButton = new ShapeActor (114, 148, 145, 43, GameColor.PURPLE_BUTTON);
        playAgainFont = new CentredFontActor("Play again",145, 296.5f, 18,Color.WHITE,"FuturaHeavy.ttf");
        playAgainFont.setPosition(playAgainFont.getX(), playAgainButton.getY() + (playAgainButton.getHeight() - playAgainFont.getHeight())/7 );
        shareFont = new CentredFontActor("Share", 164, 237, 18, Color.WHITE, "FuturaHeavy.ttf");
        shareFont.setPosition(shareFont.getX(), shareButton.getY() + (shareButton.getHeight() - shareFont.getHeight())/7 );
        backToMenuFont = new CentredFontActor("Back to Menu", 132, 176, 18, Color.WHITE, "FuturaHeavy.ttf");
        backToMenuFont.setPosition(backToMenuFont.getX(), backToMenuButton.getY() + (backToMenuButton.getHeight() - backToMenuFont.getHeight())/7 );
        yourScore = new CentredFontActor("Your score", 153, 545, 16, Color.BLACK, "FuturaBook.ttf");
        bestScore = new CentredFontActor("Best score", 154, 411, 16, Color.BLACK, "FuturaBook.ttf");
        score = new ScoreFontActor(true,160, 503, 72, Color.BLACK, "FuturaHeavy.ttf");
        highscore = new ScoreFontActor(false,160, 380, 32, Color.BLACK,"FuturaHeavy.ttf");
        //circles = new CircleActor(189,474, 18, GameColor.SECOND_CIRCLE, GameColor.FIRST_CIRCLE);
        playAgainButton.addListener(new MoveToGame());
        backToMenuButton.addListener(new MoveToMenu());

        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);

        stage.addActor(playAgainButton);
        stage.addActor(shareButton);
        stage.addActor(backToMenuButton);
        //stage.addActor(circles);
        stage.addActor(score);
        stage.addActor(playAgainFont);
        stage.addActor(shareFont);
        stage.addActor(backToMenuFont);
        stage.addActor(yourScore);
        stage.addActor(bestScore);
        stage.addActor(highscore);
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(255, 255, 255, 255);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();

    }
    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
    }
}
