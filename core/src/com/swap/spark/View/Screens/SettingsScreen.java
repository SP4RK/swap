package com.swap.spark.View.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.swap.spark.Controller.GameColorType;
import com.swap.spark.Controller.MoveTo.MoveToMenu;
import com.swap.spark.SwapGame;
import com.swap.spark.View.ImageActor;

public class SettingsScreen implements Screen {
    private Stage stage;
    private ImageActor screenBackGround;
    private ImageActor SettingsTopText;
    private Image SettingsTopCircles;
    private ImageActor BackToMenu;
    private ImageActor SettingsColorText;
    private ImageActor SettingsColorActive;
    private ImageActor SettingsColorInactive;
    private ImageActor SettingsColorBWChosen;
    private ImageActor SettingsColorBW;
    private ImageActor SettingsColorUnlock;
    private ImageActor SettingBGText;
    private ImageActor SettingBG;
    private ImageActor SettingsSound;
    private ImageActor SettingsSoundMuted;
    private Texture circlesBW = new Texture(SwapGame.getInstance().resolvePath("settings_top_circlesBW.png"));
    private Texture circlesPB = new Texture(SwapGame.getInstance().resolvePath("settings_top_circles.png"));


    public SettingsScreen(SpriteBatch batch) {

        screenBackGround = new ImageActor(new Texture(SwapGame.getInstance().resolvePath("white_background.png")), 0 , 0);
        SettingsTopText = new ImageActor(new Texture(SwapGame.getInstance().resolvePath("settings_top_text.png")),130, 581, 116, 72);
        SettingsTopCircles = new Image(circlesPB);
        SettingsTopCircles.setSize(SettingsTopCircles.getWidth() * SwapGame.getInstance().getPpuX(), SettingsTopCircles.getHeight() * SwapGame.getInstance().getPpuY());
        SettingsTopCircles.setPosition(151 * SwapGame.getInstance().getPpuX(),583 * SwapGame.getInstance().getPpuY());
        BackToMenu = new ImageActor(new Texture(SwapGame.getInstance().resolvePath("back_to_menu_button.png")),20, 606, 17, 29);
        BackToMenu.addListener(new MoveToMenu());
        SettingsColorText = new ImageActor(new Texture(SwapGame.getInstance().resolvePath("settings_text.png")),42, 530, 175, 18 );
        SettingsColorActive = new ImageActor(new Texture(SwapGame.getInstance().resolvePath("settings_color_chosen.png")),31, 421, 75, 85 );
        SettingsColorBW = new ImageActor(new Texture(SwapGame.getInstance().resolvePath("settings_color_bw.png")),157, 444, 62, 39);
        SettingsColorBW.addListener(new BWChoose());
        SettingsColorBWChosen = new ImageActor(new Texture(SwapGame.getInstance().resolvePath("settings_color_bw_chosen.png")),158, 421, 75, 85);
        SettingsColorBWChosen.setVisible(false);
        SettingsColorInactive = new ImageActor(new Texture(SwapGame.getInstance().resolvePath("settings_color_original_unchosen.png")),31, 444, 62, 39);
        SettingsColorInactive.setVisible(false);
        SettingsColorInactive.addListener(new OriginalChoose());
        SettingsColorUnlock = new ImageActor(new Texture(SwapGame.getInstance().resolvePath("settings_color_unlock.png")),294, 420, 60, 63 );
        SettingBGText = new ImageActor(new Texture(SwapGame.getInstance().resolvePath("settings_background_text.png")),21, 332, 185, 24);
        SettingBG = new ImageActor(new Texture(SwapGame.getInstance().resolvePath("settings_background_active.png")),44, 200, 290, 122);
        SettingsSound = new ImageActor(new Texture(SwapGame.getInstance().resolvePath("settings_sound.png")),161, 20, 50, 50);
        SettingsSound.addListener(new VolumeChangeOff());
        SettingsSoundMuted = new ImageActor(new Texture(SwapGame.getInstance().resolvePath("settings_sound_muted.png")),161, 20, 50, 50);
        SettingsSoundMuted.addListener(new VolumeChangeOn());
        SettingsSoundMuted.setVisible(false);

        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);

        stage.addActor(screenBackGround);
        stage.addActor(SettingsTopCircles);
        stage.addActor(SettingsTopText);
        stage.addActor(BackToMenu);
        stage.addActor(SettingsColorText);
        stage.addActor(SettingsColorActive);
        stage.addActor(SettingsColorBW);
        stage.addActor(SettingsColorUnlock);
        stage.addActor(SettingsColorBWChosen);
        stage.addActor(SettingsColorInactive);
        stage.addActor(SettingBGText);
        stage.addActor(SettingBG);
        stage.addActor(SettingsSound);
        stage.addActor(SettingsSoundMuted);
}

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(255, 255, 255, 255);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }


    @Override
    public void dispose() {
        stage.dispose();
    }

    class VolumeChangeOff extends ClickListener {
        public void clicked (InputEvent event, float x, float y) {
            SettingsSoundMuted.setVisible(true);
        }
    }
    class VolumeChangeOn extends ClickListener {
        public void clicked (InputEvent event, float x, float y) {
            SettingsSoundMuted.setVisible(false);
        }
    }
    class BWChoose extends ClickListener {
        public void clicked (InputEvent event, float x, float y) {
            SwapGame.getInstance().setGameColorType(GameColorType.BLACK_WHITE);
            SettingsColorActive.setVisible(false);
            SettingsColorBWChosen.setVisible(true);
            SettingsColorBW.setVisible(false);
            SettingsColorInactive.setVisible(true);
            SettingsTopCircles.setDrawable(new SpriteDrawable(new Sprite(circlesBW)));

        }
    }
    class OriginalChoose extends ClickListener {
        public void clicked (InputEvent event, float x, float y) {
            SwapGame.getInstance().setGameColorType(GameColorType.PINK_BLUE);
            SettingsColorActive.setVisible(true);
            SettingsColorBWChosen.setVisible(false);
            SettingsColorBW.setVisible(true);
            SettingsColorInactive.setVisible(false);
            SettingsTopCircles.setDrawable(new SpriteDrawable(new Sprite(circlesPB)));
        }
    }
}
