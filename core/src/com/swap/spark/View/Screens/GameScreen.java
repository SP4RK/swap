package com.swap.spark.View.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.swap.spark.Controller.KeyboardController;
import com.swap.spark.Controller.TouchScreenController;
import com.swap.spark.Model.UI;
import com.swap.spark.Model.World;
import com.swap.spark.SwapGame;

import java.io.IOException;

public class GameScreen implements Screen {
    private World world;
    private UI ui;
    private com.swap.spark.Model.Pause pause;
    private InputMultiplexer multiplexer;
    KeyboardController controller;
    TouchScreenController controller2;
    public GameScreen(SpriteBatch batch) throws IOException {
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        world = new World(new ScreenViewport(camera), batch);
        ui = new UI(new ScreenViewport(camera), batch);
        pause = new com.swap.spark.Model.Pause(new ScreenViewport(camera), batch);
        controller = new KeyboardController(world.getPlayer());
        controller2 = new TouchScreenController(world.getPlayer());
        multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(ui);
        multiplexer.addProcessor(controller);
        multiplexer.addProcessor(new GestureDetector(controller2));
        multiplexer.addProcessor(world);
        multiplexer.addProcessor(pause);
    }

    @Override
    public void show() {
        try {
            world.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Gdx.input.setInputProcessor(multiplexer);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        switch (SwapGame.getInstance().gameState){
            case GAME_RUNNING:
                pause.dispose();
                Gdx.input.setInputProcessor(multiplexer);
                world.act(delta);
                world.draw();
                ui.draw();
                break;
            case GAME_PAUSED:
                //world.draw();
                Gdx.input.setInputProcessor(pause);
                pause.draw();
                break;
            case GAME_RESTARTED:
                try {
                    pause.dispose();
                    world.restart();
                    world.start();
                    Gdx.input.setInputProcessor(multiplexer);
                    world.act(delta);
                    world.draw();
                    ui.draw();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            case NOT_IN_GAME:

        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
        Gdx.input.setInputProcessor(multiplexer);
    }

    @Override
    public void hide() {
        world.restart();
    }

    @Override
    public void dispose() {
    }
}

