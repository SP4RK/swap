package com.swap.spark.View.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.swap.spark.Controller.MoveTo.MoveToMenu;
import com.swap.spark.SwapGame;
import com.swap.spark.View.BackButtonActor;
import com.swap.spark.View.CentredFontActor;
import com.swap.spark.View.FontActor;
import com.swap.spark.View.GameColor;
import com.swap.spark.View.ImageActor;

public class AboutScreen implements Screen {
    private Stage stage;

    private FontActor HowTo;
    private FontActor Tap;
    private FontActor Swipe;
    private FontActor Note;
    private FontActor Coded;
    private FontActor Designed;
    private ImageActor AboutTop;
    private ImageActor BackToMenu;
    private BackButtonActor BackButton;


    public AboutScreen(SpriteBatch batch) {

        HowTo = new FontActor("HOW TO PLAY",21, 550, 28, GameColor.FIRST_CIRCLE,"FuturaHeavy.ttf");
        Tap = new FontActor("Tap to swap circles:",21, 493, 24,Color.BLACK,"FuturaBook.ttf");
        Swipe = new FontActor("Swipe to move them left or right:",21, 301, 24,Color.BLACK,"FuturaBook.ttf");
        Note = new FontActor("Note: you can swap the circles only \nbefore you moved them.",21, 127, 24,Color.BLACK,"FuturaBook.ttf");
        Coded = new FontActor("Coded by Sergey 'SP4RK' Tynyanov",21, 50, 18,GameColor.FIRST_CIRCLE,"FuturaBook.ttf");
        Designed = new FontActor("Designed by Michail 'BunnyArtworks' Segeda",21, 23, 18,GameColor.FIRST_CIRCLE,"FuturaBook.ttf");
        AboutTop = new ImageActor(new Texture(SwapGame.getInstance().resolvePath("about_top.png")),144, 581, 89, 72 );
        BackToMenu = new ImageActor(new Texture(SwapGame.getInstance().resolvePath("back_to_menu_button.png")),20, 606, 17, 29);
        BackButton = new BackButtonActor(21, 623);
        BackButton.addListener(new MoveToMenu());
        //About = new ImageActor(new Texture(SwapGame.getInstance().resolvePath("about.png")),10, 10,355, 560);

        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);


        stage.addActor(HowTo);
        stage.addActor(Tap);
        stage.addActor(Swipe);
        stage.addActor(Note);
        stage.addActor(Coded);
        stage.addActor(Designed);
        stage.addActor(AboutTop);
        stage.addActor(BackButton);
        //stage.addActor(About);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(255, 255, 255, 255);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
