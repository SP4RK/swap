package com.swap.spark.View.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.swap.spark.Controller.MoveTo.MoveToAbout;
import com.swap.spark.Controller.MoveTo.MoveToGame;
import com.swap.spark.Controller.MoveTo.MoveToSettings;
import com.swap.spark.SwapGame;
import com.swap.spark.View.CircleActor;
import com.swap.spark.View.CentredFontActor;
import com.swap.spark.View.GameColor;
import com.swap.spark.View.ImageActor;
import com.swap.spark.View.ShapeActor;

public class GameMenuScreen implements Screen {
    private Stage stage;
    private ShapeActor playButton;
    private ShapeActor settingsButton;
    private ShapeActor aboutButton;
    private CircleActor circles;
    private CentredFontActor swapFont;
    private CentredFontActor playFont;
    private CentredFontActor settingsFont;
    private CentredFontActor aboutFont;
    private ImageActor SettingsSound;
    private ImageActor SettingsSoundMuted;

    public GameMenuScreen(SpriteBatch batch) {
        playButton = new ShapeActor(114, 268, 145, 43, GameColor.BLUE_BUTTON);
        settingsButton = new ShapeActor(114, 208, 145, 43, GameColor.GRAY_BUTTON);
        aboutButton = new ShapeActor (114, 148, 145, 43, GameColor.PINK_BUTTON);
        playFont = new CentredFontActor("Play",170.5f, 296.5f, 18,Color.WHITE,"FuturaHeavy.ttf");
        playFont.setPosition(playFont.getX(), playButton.getY() + (playButton.getHeight() - playFont.getHeight())/7 );
        settingsFont = new CentredFontActor("Settings", 154.5f, 237, 18, Color.WHITE, "FuturaHeavy.ttf");
        settingsFont.setPosition(settingsFont.getX(), settingsButton.getY() + (settingsButton.getHeight() - settingsFont.getHeight())/7 );
        aboutFont = new CentredFontActor("About", 163, 176, 18, Color.WHITE, "FuturaHeavy.ttf");
        aboutFont.setPosition(aboutFont.getX(), aboutButton.getY() + (aboutButton.getHeight() - aboutFont.getHeight()) / 7);
        playButton.addListener(new MoveToGame());
        settingsButton.addListener(new MoveToSettings());
        aboutButton.addListener(new MoveToAbout());
        SettingsSound = new ImageActor(new Texture(SwapGame.getInstance().resolvePath("settings_sound.png")),161, 20, 50, 50);
        SettingsSound.addListener(new VolumeChangeOff());
        SettingsSoundMuted = new ImageActor(new Texture(SwapGame.getInstance().resolvePath("settings_sound_muted.png")),161, 20, 50, 50);
        SettingsSoundMuted.addListener(new VolumeChangeOn());
        SettingsSoundMuted.setVisible(false);

        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);

        stage.addActor(playButton);
        stage.addActor(settingsButton);
        stage.addActor(aboutButton);
        stage.addActor(playFont);
        stage.addActor(settingsFont);
        stage.addActor(aboutFont);
        stage.addActor(SettingsSound);
        stage.addActor(SettingsSoundMuted);

    }


    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void render(float delta) {
        swapFont = new CentredFontActor("S W A P", 114, 490, 42, Color.BLACK, "FuturaHeavy.ttf");
        circles = new CircleActor(189,474, 18, GameColor.FIRST_CIRCLE, GameColor.SECOND_CIRCLE);
        stage.addActor(circles);
        stage.addActor(swapFont);
        Gdx.gl.glClearColor(255, 255, 255, 255);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }
    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
    }



    class VolumeChangeOff extends ClickListener {
        public void clicked (InputEvent event, float x, float y) {
            SettingsSoundMuted.setVisible(true);
        }
    }
    class VolumeChangeOn extends ClickListener {
        public void clicked (InputEvent event, float x, float y) {
            SettingsSoundMuted.setVisible(false);
        }
    }
}
