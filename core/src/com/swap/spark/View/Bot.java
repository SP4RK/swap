package com.swap.spark.View;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.swap.spark.Controller.ColorType;
import com.swap.spark.Controller.GameState;
import com.swap.spark.Model.World;
import com.swap.spark.SwapGame;

import java.io.IOException;

public class Bot extends Player {
    float speed;
    float a = 4.2f;
    float X;
    float Y;
    float Radius;
    Player player;
    ColorType state;
    Sound boop = Gdx.audio.newSound(Gdx.files.internal(SwapGame.getInstance().resolvePath("boop.wav")));

    public Bot(float x, float y, ColorType colorType, float radius, ShapeRenderer shapeRenderer, float speed, Player player) {
        super(x, y, colorType, radius, shapeRenderer);
        X = x * SwapGame.getInstance().getPpuX();
        Y = y * SwapGame.getInstance().getPpuY();
        Radius = radius * SwapGame.getInstance().getPpuX();
        this.speed = speed;
        this.player = player;
        state = ColorType.getRandom();
        setName("bot");
    }

    public Rectangle getBorders() {
        return new Rectangle(getX() - 4 * Radius, getY() - Radius, getX() + 4 * Radius, getY() + Radius);
    }

    public void drawCircle(ShapeRenderer shape, float a, float x, float y, float radius, Color cl){
        shape.begin(ShapeRenderer.ShapeType.Filled);
        shape.setColor(cl);
        this.a = a;
        shape.circle(x, y, Radius);
        shape.end();
    }

    public void draw(Batch batch, float parentAlpha){
        shape.setProjectionMatrix(batch.getProjectionMatrix());
        batch.end();
        switch(state){
            case BLUE_PINK:
                setState(ColorType.BLUE_PINK);
                drawCircle(shape,a, getX()-a*radius, getY(), radius, GameColor.FIRST_CIRCLE);
                drawCircle(shape,a, getX()+a*radius, getY(), radius, GameColor.SECOND_CIRCLE);
                break;
            case PINK_BLUE:
                setState(ColorType.PINK_BLUE);
                drawCircle(shape,a, getX()-a*radius, getY(), radius, GameColor.SECOND_CIRCLE);
                drawCircle(shape,a, getX()+a*radius, getY(), radius, GameColor.FIRST_CIRCLE);
                break;
            case BLUE_TRANSPARENT:
                setState(ColorType.BLUE_TRANSPARENT);
                drawCircle(shape,a, getX()-a*radius, getY(), radius, GameColor.FIRST_CIRCLE);
                break;
            case TRANSPARENT_BLUE:
                setState(ColorType.TRANSPARENT_BLUE);
                drawCircle(shape, a, getX() + a * radius, getY(), radius, GameColor.FIRST_CIRCLE);
                break;
            case PINK_TRANSPARENT:
                setState(ColorType.PINK_TRANSPARENT);
                drawCircle(shape,a, getX()-a*radius, getY(), radius, GameColor.SECOND_CIRCLE);
                break;
            case TRANSPARENT_PINK:
                setState(ColorType.TRANSPARENT_PINK);
                drawCircle(shape,a, getX()+a*radius, getY(), radius, GameColor.SECOND_CIRCLE);
                break;
        }
        batch.begin();
    }

    public void act(float delta){
            moveBy(0, -speed * delta);

        if (getBorders().overlaps(player.getBorders())){
            if(getState() == player.getState()) {
                boop.play();
                remove();
                SwapGame.getInstance().setScore(SwapGame.getInstance().getScore() + 1);
            }
            else
                try {
                    gameOver();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        if(getY() <= 0) {
            try {
                ((World)getStage()).restart();
                gameOver();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void gameOver() throws IOException {
        SwapGame.getInstance().gameState = GameState.GAME_OVER;
        SwapGame.getInstance().setHighScore();
        SwapGame.getInstance().moveToGameOver();

    }
}
