package com.swap.spark.View;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.swap.spark.SwapGame;

/**
 * Created by SP4RK on 22.05.2016.
 */
public class BackButtonActor extends Actor{

    ShapeRenderer shapeRenderer;
    private float X;
    private float Y;
    private float X2;
    private float Y2;
    private Touchable touchable = Touchable.enabled;

    public BackButtonActor (float x, float y){
        shapeRenderer = new ShapeRenderer();
        X = x;
        Y = y;
        X *= SwapGame.getInstance().getPpuX();
        Y *= SwapGame.getInstance().getPpuY();
        X2 = 17 * SwapGame.getInstance().getPpuX();
        Y2 = 14 * SwapGame.getInstance().getPpuY();
        setPosition(X, Y);
    }

    public void draw (Batch batch, float parentAlpha){
        batch.end();
        shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.BLACK);
        shapeRenderer.rectLine(X, Y-1.5f, X + X2, Y + Y2, 4 * SwapGame.getInstance().getPpuX());
        shapeRenderer.rectLine(X, Y+1.5f, X + X2, Y - Y2, 4 * SwapGame.getInstance().getPpuX());
        shapeRenderer.end();
        batch.begin();
    }
    public Actor hit (float x, float y, boolean touchable) {
        if (touchable && this.touchable != Touchable.enabled) return null;
        return x >= 0 && x < X + X2 && y >= 0 - Y2 && y < Y? this : null;
    }
}
