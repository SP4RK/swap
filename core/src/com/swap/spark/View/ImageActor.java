package com.swap.spark.View;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.swap.spark.SwapGame;

public class ImageActor extends Actor {
    private TextureRegion img;

    public ImageActor (TextureRegion img, float x, float y, float width, float height){
        this.img = img;
        setPosition(SwapGame.getInstance().getPpuX() * x, SwapGame.getInstance().getPpuY()*y);
        setSize(SwapGame.getInstance().getPpuX()* width, SwapGame.getInstance().getPpuY()* height);
    }
    public ImageActor (TextureRegion img, float x, float y){
        this(img, x, y, img.getRegionWidth(), img.getRegionHeight());
    }
    public ImageActor (Texture img, float x, float y){
        this (new TextureRegion(img), x, y, img.getWidth(), img.getHeight());
    }
    public ImageActor(Texture img, float x, float y, float width, float height){
        this(new TextureRegion(img), x, y, width, height);
    }

    public void draw (Batch batch, float parentAlpha){
        batch.draw(img, getX(), getY(), getWidth(), getHeight());
    }
}
