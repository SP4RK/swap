package com.swap.spark.View;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.swap.spark.SwapGame;

public class ScoreFontActor extends Actor {
    private BitmapFont font;
    private FreeTypeFontGenerator generator;
    private FreeTypeFontGenerator.FreeTypeFontParameter parameter;
    private GlyphLayout layout;
    public float Size;
    private boolean score;

    public ScoreFontActor(boolean score, float posX, float posY, int size, Color color, String fontName) {
        generator = new FreeTypeFontGenerator(Gdx.files.internal(SwapGame.getInstance().resolvePath(fontName)));
        parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        Size = size * SwapGame.getInstance().getPpuX();
        parameter.size = (int)Size;
        parameter.color = color;
        font = new BitmapFont();
        this.score = score;
        font = generator.generateFont(parameter);
        generator.dispose();
        setPosition(posX, posY * SwapGame.getInstance().getPpuX());
        layout = new GlyphLayout();
    }

    @Override
    public void draw(Batch batch, float alpha) {
        if (score) layout.setText(font, String.format("%d", SwapGame.getInstance().Score));
        else layout.setText(font, String.format("%d", SwapGame.getInstance().HighScore));
        font.draw(batch, layout, (Gdx.graphics.getWidth()-layout.width)/2, getY());
    }
}

