package com.swap.spark.View;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.swap.spark.SwapGame;

public class CentredFontActor extends Actor {
    private String str;
    private BitmapFont font;
    private FreeTypeFontGenerator generator;
    public float Size;
    private GlyphLayout layout;
    private FreeTypeFontGenerator.FreeTypeFontParameter parameter;

    public CentredFontActor(String fontText, float posX, float posY, int size, Color color, String fontName) {
        generator = new FreeTypeFontGenerator(Gdx.files.internal(SwapGame.getInstance().resolvePath(fontName)));
        parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        Size = size * SwapGame.getInstance().getPpuX();
        parameter.size = (int)Size;
        parameter.color = color;
        font = new BitmapFont();
        font = generator.generateFont(parameter);
        generator.dispose();
        layout = new GlyphLayout();
        this.str = fontText;
        layout.setText(font, str);
        setPosition(posX, posY * SwapGame.getInstance().getPpuX());
    }

    @Override
    public void draw(Batch batch, float alpha) {
        font.draw(batch, layout, (Gdx.graphics.getWidth()-layout.width)/2, getY());
    }
}
