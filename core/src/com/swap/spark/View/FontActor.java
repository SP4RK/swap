package com.swap.spark.View;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.swap.spark.SwapGame;

/**
 * Created by SP4RK on 22.05.2016.
 */
public class FontActor extends Actor {
    private String str;
    private BitmapFont font;
    private FreeTypeFontGenerator generator;
    public float Size;
    private FreeTypeFontGenerator.FreeTypeFontParameter parameter;

    public FontActor(String fontText, float posX, float posY, int size, Color color, String fontName) {
        generator = new FreeTypeFontGenerator(Gdx.files.internal(SwapGame.getInstance().resolvePath(fontName)));
        parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        Size = size * SwapGame.getInstance().getPpuX();
        parameter.size = (int)Size;
        parameter.color = color;
        font = new BitmapFont();
        font = generator.generateFont(parameter);
        generator.dispose();
        this.str = fontText;
        setPosition(posX, posY * SwapGame.getInstance().getPpuX());
    }

    @Override
    public void draw(Batch batch, float alpha) {
        font.draw(batch, str, getX(), getY());
    }
}
