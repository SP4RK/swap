package com.swap.spark.Controller;

import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.swap.spark.SwapGame;
import com.swap.spark.View.Player;

public class TouchScreenController implements GestureDetector.GestureListener {
    Player player;
    float DeltaX;

    public TouchScreenController(Player player) {
        this.player = player;
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        if (!SwapGame.getInstance().Pause){
        player.swap();
        }
        return false;

    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        DeltaX = deltaX;
        return true;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        if (!SwapGame.getInstance().Pause) {
            if (DeltaX > 0)
                player.swipeRight();
            if (DeltaX < 0)
                player.swipeLeft();
        }
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }
}
