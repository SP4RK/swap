package com.swap.spark.Controller;

public enum GameState {
    GAME_RESTARTED,
    GAME_READY,
    GAME_RUNNING,
    GAME_PAUSED,
    GAME_OVER,
    NOT_IN_GAME

}
