package com.swap.spark.Controller.MoveTo;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.swap.spark.SwapGame;

public class MoveToSettings extends ClickListener {
    public void clicked (InputEvent event, float x, float y) {
        SwapGame.getInstance().moveToSettings();
    }
}
