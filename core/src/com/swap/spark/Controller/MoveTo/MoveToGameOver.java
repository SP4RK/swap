package com.swap.spark.Controller.MoveTo;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.swap.spark.Controller.GameState;
import com.swap.spark.SwapGame;

public class MoveToGameOver extends ClickListener {
    public void clicked (InputEvent event, float x, float y) {
        SwapGame.getInstance().gameState = GameState.GAME_READY;
        SwapGame.getInstance().moveToGameOver();
    }
}
