package com.swap.spark.Controller.MoveTo;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.swap.spark.Controller.GameState;
import com.swap.spark.SwapGame;

public class MoveToMenu extends ClickListener {
    public void clicked (InputEvent event, float x, float y) {
        SwapGame.getInstance().gameState = GameState.NOT_IN_GAME;
        SwapGame.getInstance().moveToGameMenu();
    }
}
