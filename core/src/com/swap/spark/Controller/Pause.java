package com.swap.spark.Controller;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.swap.spark.SwapGame;

public class Pause extends ClickListener {
    public void clicked (InputEvent event, float x, float y) {
        SwapGame.getInstance().gameState = GameState.GAME_PAUSED;
        SwapGame.getInstance().pause();
    }
}
