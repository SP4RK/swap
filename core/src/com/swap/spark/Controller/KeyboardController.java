package com.swap.spark.Controller;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.swap.spark.SwapGame;
import com.swap.spark.View.Player;

public class KeyboardController implements InputProcessor  {
    Player player;
    public KeyboardController(Player player){
        this.player = player;
    }
    @Override
    public boolean keyDown(int keycode) {
        if (!SwapGame.getInstance().Pause) {
            switch (keycode) {
                case Input.Keys.LEFT:
                    player.swipeLeft();
                    break;
                case Input.Keys.RIGHT:
                    player.swipeRight();
                    break;
                case Input.Keys.SPACE:
                    player.swap();
                    break;
            }
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
