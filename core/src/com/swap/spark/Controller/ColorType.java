package com.swap.spark.Controller;

public enum ColorType {
    PINK_BLUE,
    PINK_TRANSPARENT,
    BLUE_PINK,
    BLUE_TRANSPARENT,
    TRANSPARENT_BLUE,
    TRANSPARENT_PINK;

    public static ColorType getRandom() {
        return values()[(int) (Math.random()*values().length)];
    }
}
