package com.swap.spark.Controller;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.swap.spark.SwapGame;
import com.swap.spark.Model.World;

public class RestartGame extends ClickListener {
    public void clicked (InputEvent event, float x, float y) {
        SwapGame.getInstance().gameState = GameState.GAME_RESTARTED;
        SwapGame.getInstance().unpause();

    }
}
