package com.swap.spark.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.swap.spark.SwapGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		System.setProperty("user.name","English");
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.samples = 15;
		config.title = "Swap";
		config.width = 540;
		config.height = 960;
        config.addIcon("android\\assets\\icon.png", Files.FileType.Internal);
		//config.width = 373;
		//config.height = 665;
		new LwjglApplication(SwapGame.getInstance(), config);
	}
}
